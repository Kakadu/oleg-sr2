## Running 

### Client

http://localhost:3000/index.html

### Server

```
make es
```

## Building 

### Dependencies:
```
npm install -g bs-platform
npm install socket.io express reason-react js-priority-queue
npm install socket.io-client
npm install --save-dev webpack
npm install git+https://github.com/reasonml-community/bs-socket.io.git
npm link bs-platform
```

# Collect JSs before running (should be started at background)
```
make webpack
```

# Watch -- run in background while working in IDE

```
make watch
```

