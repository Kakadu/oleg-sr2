type amount_t = int;
type price_t = int;

type planet_id_t = int;

type planet_info =
  { pl_id: planet_id_t
  , pl_title: string
  , pl_coords: (int,int)
  }
type ship_info =
  { si_id: int
  , si_coords: (int,int)
  };

/* type ship_info_extended =
  { sie_id: int
  , sie_caro: list((int, amount_t, price_t))
  } */
type planet_info_extended =
  { pie_id: planet_id_t
  , pie_title: string
  , pie_cargo: list((int, amount_t, price_t))
  }

type shared = [
  /* | `Message(dataT)
  | `MessageOnEnter(data2T) */
  | `PongMsg
  | `PingMsg
  | `PauseSimulation
  | `UnpauseSimulation
];

type from_client =
  [ `Hi
  /* | `Ask_ship_info(int) */
  | `Ask_planet_info(int)
  ];
type from_server =
  [ `All_ships (list(ship_info))
  | `All_planets(list(planet_info))
  | `Planet_info(planet_info_extended)
  /* | `Ship_info */
  ];

type serverToClient = [ from_server | shared ];
type clientToServer = [ from_client | shared ];
