type ('a,'b) heapElement = {
  key: 'a;
  value: 'b;
  }
type ('a, 'b) t
exception EmptyQueue
exception HasHigherPriority

val create : ('a -> 'a -> bool) -> ('a,'b) t
val add : 'a -> 'b -> ('a,'b) t -> unit
val extract : ('a,'b) t -> ('a,'b) heapElement
val head : ('a,'b) t -> ('a,'b) heapElement
val size : ('a,'b) t -> int
val decrease_root_priority : 'a -> ('a,'b) t -> unit
val inspect : ('a,'b) t -> string