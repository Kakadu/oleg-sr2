type t('a);

[@bs.new] [@bs.module]
external priorityQueue: unit => t('a) = "PriorityQueue";

[@bs.new] [@bs.module]
external create_: 'info => t('a) =
  "../node_modules/js-priority-queue/priority-queue.min.js";

let create: (('a, 'a) => int) => t('a) =
  cmp => {
    let arg = Js.Dict.empty();
    let _ = Js.Dict.set(arg, "comparator", cmp);
    create_(arg);
  };

[@bs.get] external length: t('a) => int = "";
[@bs.send] external peek: t('a) => 'a = "";
[@bs.send] external queue: (t('a), 'a) => unit = "";
[@bs.send] external pop: t('a) => 'a = "dequeue";
[@bs.send] external dequeue: t('a) => 'a = "dequeue";
let add = queue;
