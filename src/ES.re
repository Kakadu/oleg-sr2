module Path = {
  type pathT;
  [@bs.module "path"] [@bs.splice]
  external join: array(string) => string = "";
};
module Express = {
  type expressT;
  [@bs.module] external express: unit => expressT = "";
  [@bs.send] external use: (expressT, string) => unit = "use";
  [@bs.module "express"] external static: string => string = "static";
  type responseT;
  [@bs.send] external sendFile: (responseT, string, 'a) => unit = "sendFile";
  [@bs.send]
  external get: (expressT, string, ('a, responseT) => unit) => unit = "get";
};

module Http = {
  type http;
  [@bs.module "http"] external create: Express.expressT => http = "Server";
  [@bs.send] external listen: (http, int, unit => unit) => unit = "";
};

[@bs.val] external __dirname: string = "";

let app = Express.express();
let () = Express.use(app) @@ Express.static(Path.join([|__dirname, ".."|]));

let () =
  Express.get(app, "/", (req, res) => {
    Js.log(req);
    Express.sendFile(res, "index.html", {"root": __dirname})
  });
  /*
  Express.get(app, "/node_modules/fs/:src", (_req, res) => {
    let f = [%raw {| _req.params.src |}];
    Express.sendFile(res, "fs/" ++ f, {"root": __dirname})
  });     */

module MyServer = BsSocket.Server.Make(ExampleMessages);
[@bs.send]
external serveClient:
  (BsSocket.Server.serverT, (bool => list(string) => unit)) => unit =
  "clients";

let getClientById : (BsSocket.Server.serverT, string) => option (BsSocket.Server.socketT) = [%raw {|
  function(s, id) {
    if (!id) {
      console.log("no socket with this id:" + id);
      return undefined;
    }
    var ans =  s.sockets.sockets[id];
    if (ans) return ans; else return undefined /* None */;
  }
|}];


/** Dumb demo simulation */
module SimpleSim /*: Utils.SIMULATION_TYPE */ = {
  type step_result = Utils.info_to_send;
  let step_index = ref(0);
  let cur_step = () => step_index^;

  let init = () => ();
  let step = () => {
    let si = {
      let si_id = 1;
      switch (step_index^ mod 4) {
        | 0 => ExampleMessages.({ si_id, si_coords: (300, 200) })
        | 1 => ExampleMessages.({ si_id, si_coords: (200, 300) })
        | 2 => ExampleMessages.({ si_id, si_coords: (100, 200) })
        | 3 => ExampleMessages.({ si_id, si_coords: (200, 100) })
        | _ => assert(false)
      };
    };
    incr(step_index);
    ([si],[])
  };
};

/* Change here to switch to another simulation engine */
module Simulation: Utils.SIMULATION_TYPE =
  /* SimpleSim; */
  Sim.Engine;

let http = Http.create(app);
let io = MyServer.createWithHttp(http);
let socket = MyServer.onConnect(io, socket => {
  print_endline("Got a connection!");
  let socket = MyServer.Socket.join(socket, "someRoom");
  MyServer.Socket.on(socket, fun
    /* | Shared(message) => {
        Socket.broadcast(socket, message);
        Socket.emit(socket, message);
      } */
    | `Hi => {
        Js.log("oh, hi client.");
      }
    | `PauseSimulation => Utils.Js.warn("Pause simulation not implemented")
    | `UnpauseSimulation => Utils.Js.warn("UnpauseSimulation not implemented")
    | `PingMsg => Utils.Js.warn("PingMsg not implemented")
    | `PongMsg => Utils.Js.warn("PongMsg not implemented")
    | `Ask_planet_info(pid) => {
        switch (Simulation.planet_ext_info(pid)) {
          | None => ()
          | Some(info) => MyServer.Socket.emit(socket, `Planet_info(info))
        }
    }
  );
});

let () = {
  let send_all = msg => {
    serveClient(io, _ => xs => {
      List.iter(cid => {
        switch (getClientById(io,cid)) {
        | None    => ()
        | Some(s) => List.iter(MyServer.Socket.emit(s), msg);
        };
      }, xs)
    });
  };

  let loop = () => {
    print_endline("loop");
    let (ships,planets) = Simulation.step();
    send_all([`All_planets(planets), `All_ships(ships)]);
  };

  let () = Simulation.init();
  let _ = Js.Global.setInterval(loop, 2*1000)
}


let () = Http.listen(http, 3000, () => print_endline("listening on *:3000"));
