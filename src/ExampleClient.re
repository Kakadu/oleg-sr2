/***
 * All credit goes to Cheng Lou. It was just too hard to figure out jengaboot + bucklescript for now.
 * Copy pasted from https://github.com/chenglou/reason-js
 **/
[@bs.send] external toString : Js.t('a) => string = "toString";

module Event = {
  type eventT;
  let isEnterKey: eventT => bool = [%bs.raw
    {|
    function (e) {
      return e.which === 13;
    }
  |}
  ];
};

/* Created a bunch of modules to keep things clean. This is just for demo purposes. */
module Element = {
  type elementT;
  [@bs.set] external setInnerHTML : (elementT, string) => unit = "innerHTML";
  [@bs.get] external getInnerHTML : elementT => string = "innerHTML";
  [@bs.set] external setValue : (elementT, string) => unit = "value";
  [@bs.get] external getValue : elementT => string = "value";
  [@bs.send]
  external addEventListener : (elementT, string, Event.eventT => unit) => unit =
    "addEventListener";
};

module Document = {
  [@bs.val]
  external getElementById : string => Element.elementT =
    "document.getElementById";
  [@bs.val]
  external addEventListener : (string, Event.eventT => unit) => unit =
    "document.addEventListener";
};

module Window = {
  type intervalIdT;
  [@bs.val]
  external setInterval : (unit => unit, int) => intervalIdT =
    "window.setInterval";
  [@bs.val]
  external clearInterval : intervalIdT => unit = "window.clearInterval";
};

module MyClient = BsSocket.Client.Make(ExampleMessages);

let socket = MyClient.create();

let () = MyClient.emit(socket, `Hi);

let chatarea = Document.getElementById("chatarea");

let prependLog(area, str) = {
  let innerHTML = Element.getInnerHTML(area);
  Element.setInnerHTML(area, str ++ innerHTML);
};

let sendbutton = Document.getElementById("sendbutton");

let chatinput = Document.getElementById("chatinput");

Element.addEventListener(sendbutton, "click", (_) => {
  /* MyClient.emit(
    socket,
    Shared(Message(Data(Element.getValue(chatinput)))),
  ); */
  ()
});

Document.addEventListener("keyup", e =>
  if (Event.isEnterKey(e)) {
    /* MyClient.emit(
      socket,
      Shared(MessageOnEnter(Element.getValue(chatinput))),
    ); */
    Element.setValue(chatinput, "");
  }
);

/* *********************************************************************** */
[@bs.deriving abstract]
type scene_t = {
  preload: unit => unit,
  create:  unit => unit,
};

[@bs.deriving abstract]
type config = {
  parent: string,
  width:  int,
  height: int,
  scene:  scene_t,
};

type image;
type circle = {
  on: (string, unit => unit) => unit
};

[@bs.deriving abstract]
type gameObjectFactory =
  { image:  (. int, int, string) => image
    /* circle( [x] [, y] [, radius] [, fillColor] [, fillAlpha]) */
  , circle: (. int, int, int, int) => circle
  };

[@bs.deriving abstract]
type g = {
  scene: sceneManager
}
[@bs.deriving abstract]
and sceneManager = {
  getAt: int => scene
}
[@bs.deriving abstract]
and scene = {
  add : gameObjectFactory
};

let string_to_color : string => int = [%bs.raw {|
function(str) {
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash % (256*256*256);
  /*var colour = '#';
  for (var i = 0; i < 3; i++) {
    var value = (hash >> (i * 8)) & 0xFF;
    colour += ('00' + value.toString(16)).substr(-2);
  }
  return colour;*/
}
|}];

let default_scene: g => scene =
  [%bs.raw {|
    function(g) {
      var ans = g.scene.scenes[0];
      console.assert(ans);
      return ans;
    } |}];

[@bs.new] external createGame : config => g = "Phaser.Game";


[@bs.deriving abstract]
type data_manager = {
  count: int,
  has: string => bool,
  get: 'r . string => 'r,
  set: 'v . string => 'v => unit,
};

module Ship = {
  [@bs.deriving abstract]
  type t =  { setDataEnabled: (. unit) => unit
            , mutable x     : int
            , mutable y     : int
            , on: (. string, unit => unit) => unit
            , setInteractive: (. unit) => unit
            };

  let add_on_click : t => (unit => unit) => unit = sh => f => {
    sh->onGet(. "pointerup", () => { f(); });
  };
  let create_ship : scene => t = sc => {
    let ship : t = Obj.magic( sc->addGet->circleGet(. 100, 200, 5, 0x00ff00) );
    ship->setInteractiveGet(.);
    /* Js.log(ship); */
    setDataEnabledGet(ship)(.);
    ship->onGet(. "pointerup", () => { Js.log("clicked on ship"); });
    ship
  };
  let create_planet : (scene,ExampleMessages.planet_info) => t = (sc,pl) => {
    let (x,y) = pl.ExampleMessages.pl_coords;
    let ship : t = Obj.magic( sc->addGet->circleGet(. x, y, 20, string_to_color(pl.pl_title)) );
    ship->setInteractiveGet(.);
    /* Js.log(ship); */
    setDataEnabledGet(ship)(.);
    /* ship->onGet(. "pointerup", () => { Utils.Js.flog("clicked on planet %s", pl.ExampleMessages.pl_title); }); */
    ship
  };
  let data : t => data_manager = [%bs.raw {| function(s) { return s.data; } |}];
  /* let id : g => int = _ship => {
    let ans = [%bs.raw {| _ship.data.get("si_id") |}];
    ans;
  }; */
  let setXY: t => (int,int) => unit = ship => (x,y) => {
    ship->xSet(x);
    ship->ySet(y);
  };
};

let preload = () => {
  /* let _ = [%bs.raw {| this.load.setBaseURL('http://labs.phaser.io')     |}];
  let _ = [%bs.raw {| this.load.image('sky', 'assets/skies/space3.png') |}];
  let _ = [%bs.raw {| this.load.image('ship', 'https://static.thenounproject.com/png/281034-200.png') |}]; */
  ()
};

let create = () => {
  /* let scene1 = default_scene([%bs.raw {| this.game |}]); */
  /* Js.log(scene1); */
  /* let _sh1 = Ship.create_ship(scene1); */
  ()
};

let ship_objects: MyHashMap.t((Ship.t, ExampleMessages.ship_info)) =
  MyHashMap.make(~hintSize=13);

let planet_objects: MyHashMap.t((Ship.t, ExampleMessages.planet_info)) =
  MyHashMap.make(~hintSize=13);

let game = createGame(config( ~parent="phaser_here", ~width=640, ~height = 480,
                              ~scene = scene_t(~preload, ~create)
                            ))
;

module type INTERACTION_TYPE = {
  let on_all_ships : list(ExampleMessages.ship_info) => unit;
  let on_all_planets: (list(ExampleMessages.planet_info), ExampleMessages.planet_id_t => unit) => unit;
};

module Interactions = {
  let on_all_ships = xs => {
    /* we should iterate over all ships. If some dissappeared then we remove sprite.
      If new one appeared, then we add the one.
    */
    xs |> List.iter(fun | { ExampleMessages.si_id, si_coords: (x,y) } as sinfo => {
      let ship =
        if (MyHashMap.has(ship_objects, si_id)) {
          let (s,_) = MyHashMap.get_exn(ship_objects, si_id);
          s
        } else {
          let texture = Ship.create_ship(default_scene(game));
          MyHashMap.set(ship_objects, si_id, (texture,sinfo));
          texture;
        };
      Ship.setXY(ship)(x,y);
    });
  }

  let on_all_planets = (ps,on_clicked) => {
    ps |> List.iter(fun | { ExampleMessages.pl_id: id, pl_coords: (x,y) } as pl_info => {
      let pl =
        if (MyHashMap.has(planet_objects, id)) {
          let (s,_) = MyHashMap.get_exn(planet_objects, id);
          s
        } else {
          let texture = Ship.create_planet(default_scene(game), pl_info);
          MyHashMap.set(planet_objects, id, (texture, pl_info));
          Ship.add_on_click(texture, () => { on_clicked(id); })
          texture;
        };
      Ship.setXY(pl)(x,y);
    });
  };
}



module SubscribeSocket = (I: INTERACTION_TYPE) => {
  let on_planet_pressed = pl_id => {
    Utils.Js.flog("planet %d clicked", pl_id);
    MyClient.emit(socket, `Ask_planet_info(pl_id));
  }
  let subscribe () =
    MyClient.on(socket, fun
    | `All_ships(ss)     => I.on_all_ships(ss)
    | `All_planets(ps)   => I.on_all_planets(ps, on_planet_pressed)
    | `PauseSimulation   => assert(false)
    | `UnpauseSimulation => assert(false)
    | `PingMsg => Js.log("ping from server")
    | `PongMsg => {
        let innerHTML = Element.getInnerHTML(chatarea);
        Element.setInnerHTML(
          chatarea,
          innerHTML
          ++ "<div><span style='color:red'>Message</span>: "
          ++ ""
          ++ "</div>",
        );
      }
      | `Planet_info(info) => {
        Utils.Js.flog("planet %d is called `%s`", info.pie_id, info.pie_title )
      }
    /* | _ => {
        Js.log("unhandled case");
        assert(false);
      } */
    );
};

module SS = SubscribeSocket(Interactions);
let () = SS.subscribe();
