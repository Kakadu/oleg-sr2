/* Simulation runs here */
open Printf;
open Utils;

type time_t = int; /* index of simulation step */
module Q = {
  type t('a) = PQ.t((time_t, 'a));
  let create = () => PQ.create(((a, _), (b, _)) => a - b);
  let add = (q, k, v) => PQ.add(q, (k, v));
  let head_key = q: time_t => fst(PQ.peek(q));
  let pop = q => PQ.pop(q) |> snd;
  let is_empty = q => PQ.length(q) == 0;
  let size = PQ.length;
};

type q('v) = Q.t('v);

type point = (int, int);
type good_idx = int;
type planet_idx = int;
type tr_idx = int;

let name_of_good =
  fun
  | 1 => "food(1)"
  | 2 => "metal(2)"
  | n => Printf.sprintf("stuff%d", n);

module GoodsHash = {
  include MyHashMap;

  let to_string = (h, of_val) =>
  reduce(h, "", (acc, k, v) =>
    acc ++ Printf.sprintf("%s -> %s; ", name_of_good(k), of_val(v))
  );

  let print = (h, of_val) => Js.flog("  %s", to_string(h, of_val) );
}

module PlanetHash = GoodsHash;
module TraderHash = GoodsHash;

type planet = {
  p_id: planet_idx,
  p_title: string,
  p_coords: point,
  /* p_goods_prices:   GoodsHash.t(int),  */
  mutable p_populi: int /* population */
};

let make_planet = (~title=?, ~idx=?, ~populi=100, ~prices=?, p_coords) => {
  let p_id =
    switch (idx) {
    | None =>
      incr(Utils.last_planet_id);
      Utils.last_planet_id^;
    | Some(n) => n
    };

  let p_title =
    switch (title) {
    | None => sprintf("Planet%d", p_id)
    | Some(s) => s
    };
/*
  let p_goods_prices =
    switch (prices) {
    | None => GoodsHash.of_list([(1, 2), (2, 3)])
    | Some(xs) => GoodsHash.of_list(xs)
    }; */

  {p_title, p_id, p_coords, p_populi: populi};
};

type good_info = {
  gi_amount: int,
  gi_price: int,
};
type goods_change_strat = {
  ps_timeout: int,           /* change goods every `timeout` steps */
  ps_id: int,
  ps_goods_speed: GoodsHash.t(int) /* key=id; value=changing speed */
};
let make_gi = (~amount, ~price) => {gi_amount: amount, gi_price: price};

type planet_strat =
  | GoodsChange(goods_change_strat);

let make_goods_strat = (~idx=?, ~timeout=1, xs) => {
  let ps_id =
    switch (idx) {
    | Some(n) => n
    | None =>
      incr(Utils.last_strat_id);
      Utils.last_strat_id^;
    };
  GoodsChange({
    ps_id,
    ps_timeout: timeout,
    ps_goods_speed: GoodsHash.of_list(xs),
  });
};

let string_of_strat = ({ps_id, ps_goods_speed}) =>
  Printf.sprintf("start %d: %s", ps_id) @@
  GoodsHash.to_string(ps_goods_speed) @@
  ( n =>
      sprintf("%s%d",
        if (n >= 0) {
          "+";
        } else {
          "";
        },
        n
      )
  );

type price_t = int;
type amount = int;
type trader_idx = int;
type trader_ship_loc = TrLSpace(point) | TrLPlanet(planet_idx);
type trader_strat    = TrSBuy(planet_idx) | TrSFlyTo(point, planet_idx) | TrSDoNothing;
type trader_ship =
  { tr_id: int
  , tr_speed: int
  , tr_goods: GoodsHash.t(good_info) /* stored on ship */
  , mutable tr_loc  : trader_ship_loc
  , mutable tr_strat: trader_strat
  /* , tr_strat: trader_strat */
  };

let make_trader_ship = () => {
  incr(Utils.last_trader_id);
  { tr_id: Utils.last_trader_id^
  , tr_goods: GoodsHash.of_list([])
  , tr_loc:   TrLSpace((200,200))
  , tr_strat: TrSDoNothing
  , tr_speed: 100
  };
};

/* Game state a.k.a. the World */
type game_state = {
  mutable planets: list(planet),
  mutable planet_goods: PlanetHash.t(GoodsHash.t(good_info)),
  mutable planet_ticks: Q.t((planet_idx, planet_strat)),
  mutable trader_info:
    TraderHash.t(trader_ship),

  /* key is arrival time. value us trader + where it is located */
  /* TODO: maybe we don't need a key and will reevaluate every step */
  mutable trader_ticks: Q.t(trader_idx),
};

module World = {
  let planet_by_id_exn = (w,id) => List.find(p => (p.p_id == id), w.planets);
  let planet_by_id = (w, id) => {
    try (Some( planet_by_id_exn(w,id) )) { | Not_found => None }
  }
  let serialize_planets = w => {
    w.planets |> List.map(p => {
      { ExampleMessages.pl_id: p.p_id, pl_coords: p.p_coords, pl_title: p.p_title }
    });
  };
};

let schedule_strat = (q, curtime, (_plidx, GoodsChange(st)) as v) =>
  Q.add(q, curtime + st.ps_timeout, v);

let apply_strat = (plidx, strat, all_goods) => {
  let h =
    switch (PlanetHash.get(all_goods, plidx)) {
    | Some(h) => h
    | None =>
      let h = GoodsHash.make(~hintSize=8);
      PlanetHash.set(all_goods, plidx, h);
      h;
    };

  /* Js.log2("applying", string_of_strat(strat)); */
  let delta =
    GoodsHash.reduce(h, [], (acc, k, gi) =>
      switch (GoodsHash.get(strat.ps_goods_speed, k)) {
      | None =>
        Js.log2("  can't find incrementer for good_idx", k);
        acc;
      | Some(n) =>
        /* Js.error @@ Printf.sprintf "incrementing good %d from %d by %d" k v n; */
        [(k, {...gi, gi_amount: max(0, n + gi.gi_amount)}), ...acc]
      }
    );
  List.iter(((k, v)) => GoodsHash.set(h, k, v), delta);
  /* Js.flog("  After applying strategy %d to planet %d", strat.ps_id, plidx);
  GoodsHash.print(h, ({gi_amount, gi_price}) => {
    Printf.sprintf("%d (buy for %d)", gi_amount, gi_price);
  }); */
};

let cur_step = ref(0);

let rec gen_ticks_loop = (q, f) =>
  /* read from queue and evaluate while step suits*/
  if (!Q.is_empty(q)) {
    let k = Q.head_key(q);
    if (k <= cur_step^) {
      let r = Q.pop(q);
      let () = f(k, r);
      gen_ticks_loop(q, f);
    };
  };


let planet_ticks_loop = w =>
  gen_ticks_loop(w.planet_ticks, (k, (plidx, strt)) =>
    switch (strt) {
    | GoodsChange(s) =>
      /* let () = Js.flog("found strat %d (planet %d) to run on turn %d", s.ps_id, plidx, k); */
      apply_strat(plidx, s, w.planet_goods);
      schedule_strat(w.planet_ticks, cur_step^, (plidx, strt));
    }
  );

let buy_and_select_dest = (w,pl_id,tr_id,trader_goods) => {
  let best_prices = GoodsHash.make(13);
  /* good_id   ----> (amount, buy_price, sell_price, planet_where_sell) */
  let planet_goods = PlanetHash.get_exn(w.planet_goods, pl_id);
  let has_this_good = (gid) => {
    switch (GoodsHash.get(planet_goods, gid)) {
      | None => false
      | Some({gi_amount}) => gi_amount > 0
    }
  }
  /* iterate over all planets and decide */
  PlanetHash.forEach(w.planet_goods, (pid, goodsH) =>
    if (pid == pl_id) { /* skip current planet */ ();
    } else {
      GoodsHash.iter(goodsH, (gid, gi1) => {
        if (has_this_good(gid)) {
          let (cur_buy_price,amount) = {
            if (GoodsHash.has(best_prices, gid)) {
              let (gi_amount, _, gi_price, _) = GoodsHash.get_exn(best_prices, gid);
              (gi_price,gi_amount)
            } else {
              let {gi_amount, gi_price} = GoodsHash.get_exn(planet_goods, gid);
              (gi_price,gi_amount)
            }
          }
          if (cur_buy_price < gi1.gi_price) {
            /* we found price to sell that is large than the best found reviously */
            let buy = GoodsHash.get_exn(planet_goods,gid);
            GoodsHash.set(best_prices, gid,
              (buy.gi_amount, buy.gi_price, gi1.gi_price, pid) )
          }
        }
      });
    }
  );
  let cmpgi = ((am1, b_price1, s_price1, _), (am2, b_price2, s_price2, _)) => {
    assert(b_price1 < s_price1);
    assert(b_price2 < s_price2);
    Int.compare(am1 * (s_price1 - b_price1), am2 * (s_price2 - b_price2) );
  }
  switch (GoodsHash.max(best_prices, cmpgi)) {
  | None => None
  | Some( (_,  (0, _, _, _)) ) => None
  | Some( (gid,(amount, bprice, sprice, plidx))) =>
    let bought_goods = GoodsHash.get_exn(planet_goods, gid);
    GoodsHash.update_map(planet_goods, gid, old => {...old, gi_amount: 0} );
    GoodsHash.set(trader_goods, gid, bought_goods);

    /* Utils.Js.flog("Ship %d travels %d -> %d carrying %d of good %d.",
      tr_id, pl_id, plidx, bought_goods.gi_amount, gid
    ); */
    Some(plidx)
  };
};

let trader_ticks_loop = w => {
  /* Js.flog("traders queue has length = %d", Q.size(w.trader_ticks)); */
  gen_ticks_loop(w.trader_ticks,
    (_key, tr_id) => {
      let tr = TraderHash.get_exn(w.trader_info, tr_id);
      let strat = tr.tr_strat;
      switch (strat) {
      | TrSDoNothing => Js.warn ("trader does nothing");
      | TrSBuy(pl_id) => /* we are not planet */  {
          let trader_goods = tr.tr_goods;
          let planet_goods = PlanetHash.get_exn(w.planet_goods, pl_id);
          /* let's sell goods from trder ship */
          GoodsHash.forEach(trader_goods, (_, {gi_amount}) =>
            PlanetHash.update_map(planet_goods, _key, g =>
              {...g, gi_amount: g.gi_amount + gi_amount}
            )
          );

          /* now we decide where to fly next */
          switch (buy_and_select_dest(w,pl_id,tr_id,trader_goods)) {
          | None => {
              Js.flog("do not schedule movement of trader %d on planet %d (waiting for new goods)", tr_id, pl_id);
              Q.add(w.trader_ticks, cur_step^ + 1, tr_id);
          }
          | Some(plid) => {
              /*  schedule fly there */
              let pl = World.planet_by_id_exn(w,pl_id);
              tr.tr_strat = TrSFlyTo(pl.p_coords, plid);
              /* update location of trader */
              tr.tr_loc = TrLSpace(pl.p_coords);
              Js.flog("Ship %d leaves planet %d adn travels to planet %d", tr.tr_id, pl.p_id, plid);
              Q.add(w.trader_ticks, cur_step^ + 1, tr_id);
            }
          }
        }
      | TrSFlyTo(coords, pl_id) => {
          let dest = World.planet_by_id_exn(w, pl_id);
          switch (tr.tr_loc) {
          | TrLSpace(coords) => {
              switch (Utils.Phys.fly_to(coords, dest.p_coords, tr.tr_speed)) {
              | `Reached => {
                Js.flog("Ship %d landing on planet %d", tr.tr_id, dest.p_id);
                tr.tr_loc = TrLPlanet(pl_id);
                tr.tr_strat = TrSBuy(pl_id);
              }
              | `Flying(x0,y0) => {
                tr.tr_loc = TrLSpace((x0,y0));
                tr.tr_strat = TrSFlyTo((x0,y0), pl_id);
                Js.flog("set location of ship %d to (%d,%d)", tr.tr_id, x0, y0);
              };
              };
              Q.add(w.trader_ticks, cur_step^ + 1, tr_id);
              Js.flog("Update trader_tics. New length = %d", Q.size(w.trader_ticks) );
            }
          | TrLPlanet(_pl_id) => Js.log("should not happen");
          }
        }
      }
    }
  )
}

let step = w => {
  /* if (cur_step^ >= max_step) {
    []
  } else { */
    let () = Js.error2("==== step", cur_step^);
    let () = planet_ticks_loop(w);
    let () = trader_ticks_loop(w);

    let ships = w.trader_info |> TraderHash.filter_map2list((_,ship) => {
      let si_coords = switch (ship.tr_loc) {
        | TrLSpace (coords) => coords
        | TrLPlanet(plid) => {
            switch (World.planet_by_id(w, plid)) {
            | Some(planet) => planet.p_coords
            | None => assert(false);
            }
        }
      }
      let si_id = ship.tr_id;
      Some({ ExampleMessages.si_id, si_coords })
    });
    let planets = World.serialize_planets(w);
    incr(cur_step);
    (ships,planets)
  /* }; */
};

let init_world = () => {
  /*    3 --->   4
       /\        |
       |        \/
       2 <----  1
   */

  let p1 = make_planet(~title="AlphaCentauri", ~idx=1, (400, 400));
  let p2 = make_planet(~title="Station1",  ~idx=2, (100, 400));
  let p3 = make_planet(~title="Venus", ~idx=3,  (100, 100));
  let p4 = make_planet(~title="Mars",  ~idx=4,  (400, 100));
  let ps1 = make_goods_strat(~timeout=1, [(4, -5), (1, +5)]);
  let ps2 = make_goods_strat(~timeout=1, [(1, -3), (2, +6)]);
  let ps3 = make_goods_strat(~timeout=1, [(2, -6), (3,  5)]);
  let ps4 = make_goods_strat(~timeout=1, [(3, -5), (4, +5)]);
  let planets = [p1, p2, p3, p4];
  let planet_ticks = {
    let q = Q.create();
    Q.add(q, init_step + 1, (p1.p_id, ps1));
    Q.add(q, init_step + 1, (p2.p_id, ps2));
    Q.add(q, init_step + 1, (p3.p_id, ps3));
    Q.add(q, init_step + 1, (p4.p_id, ps4));
    q;
  };

  let trader1 = make_trader_ship();
  trader1.tr_strat = TrSBuy(p1.p_id);
  let trader_info = TraderHash.of_list([(trader1.tr_id, trader1)]);
  let trader_ticks = {
    let q = Q.create();
    Q.add(q, 1, trader1.tr_id);
    q;
  };

  let planet_goods = PlanetHash.make(~hintSize=8);
  PlanetHash.set(planet_goods, p1.p_id) @@
  GoodsHash.of_list([
    (4, make_gi(~amount=100, ~price=29)),
    (1, make_gi(~amount=100, ~price= 7)),
  ]);
  PlanetHash.set(planet_goods, p2.p_id) @@
  GoodsHash.of_list([
    (1, make_gi(~amount=100, ~price=10)),
    (2, make_gi(~amount=100, ~price=11)),
  ]);

  PlanetHash.set(planet_goods, p3.p_id) @@
  GoodsHash.of_list([
    (2, make_gi(~amount=100, ~price=13)),
    (3, make_gi(~amount=100, ~price=17)),
  ]);
  PlanetHash.set(planet_goods, p4.p_id) @@
  GoodsHash.of_list([
    (3, make_gi(~amount=100, ~price=19)),
    (4, make_gi(~amount=100, ~price=23)),
  ]);
  {planets, planet_ticks, trader_info, trader_ticks, planet_goods};
};


module Engine = {
  /* init on load */
  let w = init_world ();
  let init = () => ();

  type step_result = (list(ExampleMessages.ship_info), list(ExampleMessages.planet_info) );

  let cur_step = () => cur_step^;
  let step () = step(w)

  let planet_ext_info = pid => {
    switch (World.planet_by_id(w,pid)) {
      | None => None
      | Some(pinfo) =>
          Some({ ExampleMessages.pie_title: pinfo.p_title, pie_id: pinfo.p_id, pie_cargo: []})
    }
  }
};
