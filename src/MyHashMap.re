include Belt_HashMap.Int;
let iter = forEach;

let of_list = xs => {
  let h = make(~hintSize=8);
  List.iter(((k, v)) => set(h, k, v), xs);
  h;
};
let update_map = (h, k, f) =>
  switch (get(h, k)) {
  | None => ()
  | Some(v) => set(h, k, f(v))
  };

let get_exn = (h, k) =>
  switch (get(h, k)) {
  | Some(r) => r
  | None => raise(Not_found)
  };

/* let to_string = (h, of_val) =>
  reduce(h, "", (acc, k, v) =>
    acc ++ Printf.sprintf("%s -> %s; ", name_of_good(k), of_val(v))
  );
let print = (h, of_val) => Js.log @@ to_string(h, of_val); */

/* intersect, map and put results to list */
let zip_with_l = (h, h2, f) =>
  reduce(h, [], (acc, k, v1) =>
    switch (get_exn(h2, k)) {
    | None => acc
    | Some(v2) => [f(k, v1, v2), ...acc]
    }
  );

let keys_list = h => reduce(h, [], (acc, k, _) => [k, ...acc]);
let map = f => h => {
  let ans = make(~hintSize=8);
  forEach(h, (k, v) => set(ans, k, f(k,v)));
  ans;
};

let max = (h, cmp) =>
  reduce(h, None, (acc, k, v) =>
    switch (acc) {
    | None => Some((k, v))
    | Some((_, vold)) when cmp(v, vold) > 0 => Some((k, v))
    | Some(_) => acc
    }
  );

let filter_map2list = f => h => {
  reduce(h, [], (acc,k, v) => {
    switch (f(k,v)) {
      | Some(ans) => [ans, ...acc]
      | None      => acc
    }
  });
};
