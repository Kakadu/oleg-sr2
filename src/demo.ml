open Printf 
open Utils

module Js = struct 
  include Js 
  let error  = Js.Console.error
  let error2 = Js.Console.error2
  let flog fmt = Printf.ksprintf Js.log fmt 
end 

module PQ = struct 
  type 'a t

  external priorityQueue : unit -> 'a t = "PriorityQueue" [@@bs.new] [@@bs.module]
  external create_ : 'info -> 'a t = "../node_modules/js-priority-queue/priority-queue.min.js" [@@bs.new] [@@bs.module]

  let create : ('a -> 'a -> int) -> 'a t = fun cmp -> 
    let arg = Js.Dict.empty () in 
    let _ = Js.Dict.set arg "comparator" cmp in 
    create_ arg

  external length  : 'a t -> int          = "" [@@bs.get] 
  external peek    : 'a t -> 'a           = "" [@@bs.send]
  external queue   : 'a t -> 'a  -> unit  = "" [@@bs.send]
  external pop     : 'a t -> 'a           = "dequeue" [@@bs.send]
  external dequeue : 'a t -> 'a           = "dequeue" [@@bs.send]
  let add  = queue
end 

type time_t = int (* number of simulation step *)
module Q = struct  
  type 'a t = (time_t * 'a) PQ.t
  let create ()  = PQ.create (fun (a,_) (b,_) -> a-b)
  let add q k v =  PQ.add q (k,v)
  let head_key q : time_t = fst (PQ.peek q) 
  let pop q = PQ.pop q |> snd
  let is_empty q = (PQ.length q = 0)
end 


type 'v q = 'v Q.t

type point = int * int
type good_idx = int 
type planet_idx = int
type tr_idx = int

let name_of_good = function 1 -> "food(1)" | 2 -> "metal(2)" | n -> Printf.sprintf "stuff%d" n

module GoodsHash = struct 
  include Belt_HashMap.Int 
  let iter = forEach 

  let of_list xs = 
    let h = make ~hintSize:8 in 
    List.iter (fun (k,v) -> set h k v) xs;
    h
  let update_map h k f = match get h k with 
    | None -> ()
    | Some v -> set h k (f v)

  let get_exn h k = 
    match get h k with 
    | Some r -> r
    | None -> raise Not_found

  let to_string h of_val = 
    reduce h "" (fun acc k v -> acc ^ Printf.sprintf "%s -> %s; " (name_of_good k) (of_val v))
  let print h of_val = Js.log @@ to_string h of_val

  (* intersect, map and put results to list *)
  let zip_with_l h h2 f = 
    reduce h [] (fun acc k v1 -> 
      match get_exn h2 k with 
      | None -> acc 
      | Some v2 -> (f k v1 v2) :: acc)

  let keys_list h = reduce h [] (fun acc k _ -> k::acc)
  let map h f = 
    let ans = make ~hintSize:8 in 
    forEach h (fun k v -> set ans k (f v));
    ans
  let max h cmp = 
    reduce h None (fun acc k v -> 
      match acc with 
      | None -> Some (k, v)
      | Some (k2,v2) when cmp v v2 > 0 -> Some (k,v)
      | Some _ -> acc
    )
end 

module PlanetHash = GoodsHash
module TraderHash = GoodsHash

type planet = 
  {         ptitle : string
  ;         pcoords: point 
  ;         pidx   : planet_idx
  ;   pgoods_prices: int GoodsHash.t (* selling = bying *)
  ; mutable ppopuli: int       (* population *)
  }
let make_planet ?title ?idx ?(ppopuli=100) ?prices pcoords = 
  let pidx = match idx with None -> incr Utils.last_planet_id; !Utils.last_planet_id
    | Some n -> n 
  in
  let ptitle = match title with
    | None -> sprintf "Planet%d" pidx
    | Some s -> s
  in
  let pgoods_prices = match prices with 
  | None    -> GoodsHash.of_list [(1,2); (2,3)]
  | Some xs -> GoodsHash.of_list xs
  in
  { ptitle; pidx; pcoords; pgoods_prices; ppopuli } 

type good_info = { gi_amount: int; gi_price: int }
type goods_change_strat = 
  { ps_timeout: int
  ; ps_id: int
  ; ps_goods_speed:  int GoodsHash.t (* key=id; value=changing speed *)
  }
let make_gi ~amount ~price = {gi_amount = amount; gi_price = price }

type planet_strat = GoodsChange of goods_change_strat
let make_goods_strat ?idx ?(timeout=1) xs = 
  let ps_id = match idx with Some n -> n | None -> incr Utils.last_strat_id; !Utils.last_strat_id in 
  GoodsChange {ps_id; ps_timeout = timeout; ps_goods_speed = GoodsHash.of_list xs} 

let string_of_strat {ps_id; ps_goods_speed} = 
  Printf.sprintf "start %d: %s" ps_id @@ 
  GoodsHash.to_string ps_goods_speed @@ 
  (fun n -> sprintf "%s%d" (if n>=0 then "+" else "") n)

type price_t = int
type amount  = int 
type trader_idx = int
type trader_ship = 
  { tr_id : int 
  ; tr_speed: int
  ; tr_goods: good_info GoodsHash.t (* stored on ship *)
  }

let make_trader_ship () = 
  incr Utils.last_trader_id;
  { tr_id= !Utils.last_trader_id; tr_speed=0; tr_goods=GoodsHash.of_list [] }


type game_state = 
  { planets:        planet list
  ; planet_goods:   (good_info GoodsHash.t) PlanetHash.t
  ; planet_ticks:   (planet_idx * planet_strat) Q.t
  ; trader_info:    trader_ship TraderHash.t
  (* key is arrival time. value us trader + where it is located *)
  ; trader_ticks:   (trader_idx * planet_idx) Q.t 
  }

let schedule_strat q curtime ((_plidx, GoodsChange st) as v) = 
  Q.add q (curtime+st.ps_timeout) v

let apply_strat plidx strat all_goods =
  let h =
    match PlanetHash.get all_goods plidx with 
    | Some h -> h
    | None ->  
        let h = GoodsHash.make ~hintSize:8 in
        PlanetHash.set all_goods plidx h;
        h 
  in
  Js.log2 "applying" (string_of_strat strat);
  let delta = GoodsHash.reduce h [] (fun acc  k gi -> 
    match GoodsHash.get strat.ps_goods_speed k with 
    | None -> Js.log2 "can't find incrementer for good_idx" k; acc
    | Some n -> 
        (* Js.error @@ Printf.sprintf "incrementing good %d from %d by %d" k v n; *)
        (k, {gi with gi_amount = max 0 (n+gi.gi_amount)}) :: acc
        
  ) in 
  List.iter (fun (k,v) -> GoodsHash.set h k v) delta;
  Js.flog "After applying strategy %d to planet %d" strat.ps_id plidx;
  let () = GoodsHash.print h (fun {gi_amount} -> string_of_int gi_amount) in 
  ()


let rec loop curstep w = 
  let rec gen_ticks_loop q f = 
    (* read from queue and evaluate while step suits*)
    if not (Q.is_empty q) then 
    let k = Q.head_key q in
    if k<=curstep then 
      let r = Q.pop q in
      let () = f k r in 
      gen_ticks_loop q f
  in 
  let planet_ticks_loop () = 
    gen_ticks_loop w.planet_ticks (fun k (plidx, strt) -> 
      match strt with 
      | GoodsChange s -> 
        let () = Js.flog "found strat %d to run on turn %d" s.ps_id k in
        (* apply_strat plidx strt (PlanetHash.get_exn w.planet_goods plidx); *)
        apply_strat plidx s w.planet_goods;
        schedule_strat w.planet_ticks curstep (plidx, strt);
    )
  in 
  let trader_ticks_loop () = gen_ticks_loop w.trader_ticks (fun k (tridx,plidx) -> 
    (* let's sell goods from trder ship *)
    let trader_goods = (TraderHash.get_exn w.trader_info tridx).tr_goods in 
    let planet_goods = PlanetHash.get_exn w.planet_goods plidx in 

    GoodsHash.forEach trader_goods (fun k {gi_amount} -> 
      PlanetHash.update_map planet_goods k (fun g -> {g with gi_amount=g.gi_amount + gi_amount}) );
    let zip_l_with_h xs h f = 
      List.fold_left (fun acc (k,v1) -> match GoodsHash.get h k with 
        | None -> acc 
        | Some v2 -> (k, f k v1 v2) :: acc) [] xs 
    in 

    let best_prices = GoodsHash.map (PlanetHash.get_exn w.planet_goods plidx) (fun v -> (k,v,0,plidx)) in 
    
    let () = 
      PlanetHash.forEach w.planet_goods (fun pid goodsH -> 
        if pid = plidx then ()
        else 
          GoodsHash.iter goodsH (fun k gi1 -> 
            match GoodsHash.get best_prices k with 
            | Some (gidx, gi, delta, plidx) when gi1.gi_price - gi.gi_price > delta -> 
                GoodsHash.set best_prices k (gidx, gi1, gi1.gi_price - gi.gi_price, pid)
            | _ -> ()
          ))
    in

    let cmpgi gi1 gi2 = Int.compare (gi1.gi_amount*gi1.gi_price)  (gi2.gi_amount*gi2.gi_price) in
    match GoodsHash.max best_prices (fun (_,gi1,_,_) (_,gi2, _,_) -> cmpgi gi1 gi2) with 
    | Some (_,(gidx, ginfo, delta, plidx)) -> 
        let goodsH = PlanetHash.get_exn w.planet_goods plidx in 
        GoodsHash.set goodsH gidx {ginfo with gi_amount=0};
        GoodsHash.set trader_goods gidx ginfo;
        Q.add w.trader_ticks (curstep+1) (tridx, plidx)
    | None -> 
        Js.log "do not schedule movement of trader";
        ()
  )
  in
  if curstep >= max_step
  then ()
  else 
    let () = Js.error2 "==== step" curstep in
    let () = planet_ticks_loop () in    
    let () = trader_ticks_loop () in
    loop (curstep+1) w

let init_world () =
  let prices = [(1,5); (2,10)] in
  let p1 = make_planet ~title:"Earth" ~idx:1 ~prices  (2,2) in
  let p2 = make_planet ~title:"Moon"  ~idx:2 ~prices (2,-2) in
  let prices = [(1,5); (2,10)] in
  let p3 = make_planet ~title:"Venus" ~idx:3 ~prices  (-2,2) in 
  let p4 = make_planet ~title:"Mars"  ~idx:4 ~prices (-2,-2) in 
  let ps1 = make_goods_strat [(1,+5); (2, -8)] in
  let ps2 = make_goods_strat ~timeout:2 [(1,+3); (2, +2)] in
  let ps3 = make_goods_strat ~timeout:2 [(1,-6); (2, +5)] in
  let ps4 = make_goods_strat ~timeout:2 [(1,-2); (2, +1)] in
  let planets = [p1;p2;p3;p4] in
  let planet_ticks = 
    let q = Q.create () in
    Q.add q (init_step+1) (p1.pidx, ps1);
    Q.add q (init_step+2) (p2.pidx, ps2);
    q
  in

  let trader1 = make_trader_ship ()  in
  let trader_info = TraderHash.of_list [(trader1.tr_id, trader1)] in 
  let trader_ticks =  
    let q = Q.create () in 
    Q.add q 1 (trader1.tr_id, p1.pidx);
    q 
  in 
  
  let planet_goods = PlanetHash.make ~hintSize:8 in
  PlanetHash.set planet_goods p1.pidx @@ GoodsHash.of_list 
    [ (1, make_gi ~amount:100 ~price:10)
    ; (2, make_gi ~amount:100 ~price:20)
    ];
  PlanetHash.set planet_goods p2.pidx @@ GoodsHash.of_list
    [ (1, make_gi ~amount:100 ~price:10)
    ; (2, make_gi ~amount:100 ~price:20)
    ];

  PlanetHash.set planet_goods p3.pidx @@ GoodsHash.of_list
    [ (1, make_gi ~amount:100 ~price:20)
    ; (2, make_gi ~amount:100 ~price:10)
    ];
  PlanetHash.set planet_goods p4.pidx @@ GoodsHash.of_list
    [ (1, make_gi ~amount:100 ~price:20)
    ; (2, make_gi ~amount:100 ~price:10)
    ];
  { planets; planet_ticks; trader_info; trader_ticks;  planet_goods }

let () = loop 0 (init_world ())


(* module MyServer = BsSocket.Server.Make(ExampleMessages) *)
