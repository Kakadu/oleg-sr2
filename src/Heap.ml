type ('a,'b) heapElement = {
  key: 'a;
  value: 'b;}
type ('a,'b) t =
  {
  queue: ('a,'b) heapElement array ref;
  compare: 'a -> 'a -> bool;}
exception EmptyQueue
let create compare = { queue = (ref [||]); compare }
let parent =
  function
  | 0 -> None
  | index -> ((Some (((index - 1) / 2)))[@explicit_arity ])
let left index = (2 * index) + 1
let right index = 2 * (index + 1)
let swap a b queue =
  let a' = queue.(a) in
  let b' = queue.(b) in queue.(a) <- b'; queue.(b) <- a'
let key queue index = (queue.(index)).key
let rec heapify index compare queue =
  let key = key queue in
  let heap_size = Array.length queue in
  let left_index = left index in
  let right_index = right index in
  let max_priority_index = ref index in
  max_priority_index :=
    (if
       (left_index < heap_size) &&
         (compare (key left_index) (key (!max_priority_index)))
     then left_index
     else !max_priority_index);
  max_priority_index :=
    (if
       (right_index < heap_size) &&
         (compare (key right_index) (key (!max_priority_index)))
     then right_index
     else !max_priority_index);
  (let max_priority_index = !max_priority_index in
   if max_priority_index <> index
   then
     (swap max_priority_index index queue;
      heapify max_priority_index compare queue))
let rec fix_up index compare queue =
  let key = key queue in
  let parent_index = parent index in
  match parent_index with
  | ((Some (p_ind))[@explicit_arity ]) when compare (key index) (key p_ind)
      -> (swap index p_ind queue; fix_up p_ind compare queue)
  | _ -> ()
let fix_last compare queue =
  let heap_size = Array.length queue in fix_up (heap_size - 1) compare queue
let extract heap =
  match !(heap.queue) with
  | [||] -> raise EmptyQueue
  | [|head|] -> (heap.queue := [||]; head)
  | q ->
      let heap_size = Array.length q in
      let head = q.(0) in
      (swap 0 (heap_size - 1) q;
       (let q = Array.sub q 0 (heap_size - 1) in
        heapify 0 heap.compare q; heap.queue := q; head))
let add key value heap =
  let queue =
    match !(heap.queue) with
    | [||] -> [|{ key; value }|]
    | q -> Array.append q [|{ key; value }|] in
  fix_last heap.compare queue; heap.queue := queue
let head heap =
  match !(heap.queue) with | [||] -> raise EmptyQueue | q -> q.(0)
let update_priority index new_priority heap =
  let queue = !(heap.queue) in
  let current_priority = key queue index in
  let value = (queue.(index)).value in
  queue.(index) <- { key = new_priority; value };
  (let has_higher_priority = heap.compare new_priority current_priority in
   if has_higher_priority
   then fix_up index heap.compare queue
   else heapify index heap.compare queue)
exception HasHigherPriority
let decrease_root_priority new_priority heap =
  let queue = !(heap.queue) in
  let current_priority = key queue 0 in
  let has_higher_priority = heap.compare new_priority current_priority in
  if has_higher_priority
  then raise HasHigherPriority
  else update_priority 0 new_priority heap
let size heap = Array.length (!(heap.queue))
let inspect heap = Js.Array.toString (!(heap.queue))