module Js = {
  include Js;
  let error = Js.Console.error;
  let error2 = Js.Console.error2;
  let warn   = Js.Console.warn;
  let flog = fmt => Printf.ksprintf(Js.log, fmt);
};

open ExampleMessages;

let last_planet_id = ref(0);
let last_trader_id = ref(50);
let last_strat_id = ref(100);

let init_step = 0;
let max_step = 13;


type info_to_send = (list(ExampleMessages.ship_info), list(ExampleMessages.planet_info) );
module type SIMULATION_TYPE = {
  type step_result = info_to_send;
  let cur_step : unit => int;

  let init: unit => unit;
  let step: unit => step_result;

  let planet_ext_info: planet_id_t => option(planet_info_extended);
};

module List = {
  include List;

  let max = (xs, f) =>
    switch (xs) {
    | [] => None
    | [x, ...xs] =>
      Some(
        fold_left(
          (acc, e) =>
            if (f(e, acc) > 0) {
              e;
            } else {
              acc;
            },
          x,
          xs,
        ),
      )
    };

  let max_exn = (xs, f) =>
    switch (max(xs, f)) {
    | Some(x) => x
    | None => raise(Not_found)
    };
};

module Int = {
  include Js.Int;
  let compare = (a, b) =>
    if (a > b) {
      1;
    } else if (a == b) {
      0;
    } else {
      (-1);
    };
};

module Phys = {
  let fly_to = ((x1,y1), (x2,y2), speed) => {
    let sp = float_of_int(speed);
    /* fly 1 ---> 2 */
    let dist = Js.Math.sqrt(float_of_int( (x2-x1) * (x2-x1) + (y2-y1) * (y2-y1) ));
    if (dist <= sp) { `Reached } else {
      let alpha = Js.Math.atan(float_of_int(y2-y1) /. float_of_int(y2-y1) );
      let dx = float_of_int(x2-x1) *. sp /. dist;
      let dy = float_of_int(y2-y1) *. sp /. dist;
      `Flying(Js.Math.unsafe_trunc(dx +. float_of_int(x1)),
              Js.Math.unsafe_trunc(dy +. float_of_int(y1)) )
    };
  }
};
