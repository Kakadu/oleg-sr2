.PHONY: watch run clean 
all:
	yarn build

watch:
	npm start

webpack:
	npm run webpack

# old demo without client/server
run:
	node src/demo.bs.js

server:
	node src/ExampleServer.bs.js
	
es:
	node src/ES.bs.js
	
clean:
	$(RM) -f src/*.js src/*.cm*
